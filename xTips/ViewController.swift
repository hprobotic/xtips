//
//  ViewController.swift
//  xTips
//
//  Created by Johnny Pham on 2/26/16.
//  Copyright © 2016 Johnny Pham. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tipPercentageCtrl: UISegmentedControl!
    @IBOutlet weak var billField: UITextField!
    @IBOutlet weak var totalTip: UILabel!
    @IBOutlet weak var totalPay: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        totalPay.text = "$0.00"
        totalTip.text = "$0.00"
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func onEditingChanged(sender: AnyObject) {
        let tipPercentages = [0.1, 0.2, 0.3]
        let tipPercentageCurrent = tipPercentages[tipPercentageCtrl.selectedSegmentIndex]
        print(tipPercentageCurrent);
        let billAmount = NSString(string: billField.text!).doubleValue
        let tips = billAmount*tipPercentageCurrent
        let total = billAmount + tips
        
        totalTip.text = String(format: "$%.2f", tips)
        totalPay.text = String(format: "$%.2f", total)
    }
    @IBAction func onTap(sender: AnyObject) {
        view.endEditing(true)
    }
}

